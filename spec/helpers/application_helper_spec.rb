require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title" do
    let(:BASE_TITLE) { "Potepanec" }

    context "page_title is nil" do
      it "full_title is potepanec" do
        expect(full_title(nil)).to eq Const::BASE_TITLE
      end
    end

    context "page_title is empty string" do
      it "full_title is base title" do
        expect(full_title("")).to eq Const::BASE_TITLE
      end
    end

    context "page_title is not blank" do
      it "full_title is hogehoge | Potepanec" do
        expect(full_title("hogehoge")).to eq "hogehoge | #{Const::BASE_TITLE}"
      end
    end

    context "if there is no page_title argument" do
      it "full_title is base_title!" do
        expect(full_title({})).to eq Const::BASE_TITLE
      end
    end
  end
end
