require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe 'potepan/products' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'response should succeed' do
      expect(response).to have_http_status(200)
    end

    it "there should be a product name" do
      expect(response.body).to include product.name
    end

    it "there should be a product description" do
      expect(response.body).to include product.description
    end

    it "there should be a product price" do
      expect(response.body).to include product.display_price.to_s
    end

    it "there shpuld be a return index_page" do
      expect(response.body).to include "一覧ページへ戻る"
      expect(response.body).to include potepan_category_path(product.taxons.first.id)
    end

    it "there should be at least one product image" do
      product.images.each do |image|
        expect(page).to have_selector "img[src$='#{product.image.attachment(:large)}']", count: 1
        expect(page).to have_selector "img[src$='#{product.image.attachment(:small)}']", count: 1
      end
    end

    it "4 related products are displayed" do
      related_products.each do |p|
        expect(response.body).to include p.name
        expect(response.body).to include p.display_price.to_s
      end
    end
  end
end
