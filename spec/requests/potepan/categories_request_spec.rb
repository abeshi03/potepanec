require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "potepan_category" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:taxonomy) { create(:taxonomy, name: "Categories") }

    before do
      get potepan_category_path(taxon.id)
    end

    it "response should success" do
      expect(response).to have_http_status(200)
    end

    it "there should be a product name" do
      expect(response.body).to include product.name
    end

    it "there should be a product price" do
      expect(response.body).to include product.display_price.to_s
    end

    it "there should be a taxonomy name" do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include 'Brand'
      expect(response.body).to include 'Categories'
    end

    it "there should be a taxon name" do
      expect(response.body).to include taxon.name
    end

    it "there should be a taxon products count" do
      expect(response.body).to include taxon.products.count.to_s
    end
  end
end
