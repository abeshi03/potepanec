require 'rails_helper'

RSpec.describe "Product", type: :model do
  describe "related_products_test" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }

    context "related_products is 0" do
      it { expect(product.related_products(Const::RELATED_PRODUCTS_COUNT).length).to eq 0 }
    end

    context "related_products is 3" do
      let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }

      it { expect(product.related_products(Const::RELATED_PRODUCTS_COUNT).length).to eq 3 }
    end

    context "related_products is 4" do
      let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

      it { expect(product.related_products(Const::RELATED_PRODUCTS_COUNT).length).to eq 4 }
    end

    context "related_products is 5" do
      let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

      it { expect(product.related_products(Const::RELATED_PRODUCTS_COUNT).length).to eq 4 }
    end

    context "your own data is not included in related products" do
      let!(:related_products) { create(:product, taxons: [taxon]) }

      it "you shouldn't have your own id" do
        expect(product.related_products(Const::RELATED_PRODUCTS_COUNT).ids).
          not_to include product.id
      end
    end
  end
end
