require 'rails_helper'

RSpec.describe "Products", type: :system do
  describe 'potepan/products pages' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:related_products) do
      4.times.collect do |i|
        create(:product, name: "test_#{i}", price: "#{i}", taxons: [taxon])
      end
    end

    before do
      visit potepan_product_path(product.id)
    end

    it 'go to potepan_root_path' do
      find(".nav_home").click
      expect(page).to have_title "BIGBAG Store"
      expect(current_path).to eq potepan_root_path
    end

    it 'potepan/products pages display content' do
      expect(page).to have_title "#{product.name} | Potepanec"
      expect(page).to have_selector "a", text: "カートへ入れる"
      expect(page).to have_link "Home", count: 2
      expect(page).to have_content product.name, count: 3
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    it "go to potepan_category_path" do
      click_link "一覧ページへ戻る"
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "related product information" do
      expect(page).to have_content "関連商品"
      related_products.each do |p|
        expect(page).to have_content p.name
        expect(page).to have_content p.display_price
      end
    end

    it "4 related products are displayed" do
      expect(page).to have_css ".productBox", count: 4
    end

    it "related product name link test" do
      related_products.each do |p|
        click_on p.name
        expect(current_path).to eq potepan_product_path(p.id)
      end
    end

    it "related_product price link test" do
      related_products.each do |p|
        click_on p.display_price
        expect(current_path).to eq potepan_product_path(p.id)
      end
    end
  end
end
