require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  describe "potepan_category page" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:taxonomy) { create(:taxonomy) }

    before do
      visit potepan_category_path(taxon.id)
    end

    it "potepan_category page display content" do
      expect(page).to have_title "#{taxon.name} | #{Const::BASE_TITLE}"
      expect(page).to have_content "商品カテゴリー"
      expect(page).to have_content "色から探す"
      expect(page).to have_content "サイズから探す"
      expect(page).to have_content taxon.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.products.count
    end

    it "taxon_name link is go to potepan_category_path" do
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it "product_name link is go to potepan_product_path" do
      click_on product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end

    it "product_price link is go to potepan_product_path" do
      click_on product.display_price
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
