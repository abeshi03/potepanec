module Potepan::ProductDecorator
  def related_products(number)
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct.limit(number)
  end
  Spree::Product.prepend self
end
