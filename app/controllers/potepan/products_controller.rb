class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(Const::RELATED_PRODUCTS_COUNT).
      includes(master: [:images, :default_price])
  end
end
